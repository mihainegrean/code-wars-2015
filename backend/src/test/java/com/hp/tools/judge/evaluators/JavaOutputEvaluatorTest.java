package com.hp.tools.judge.evaluators;

import com.hp.tools.judge.TestUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class JavaOutputEvaluatorTest {
    private OutputEvaluator evaluator;

    @Before
    public void setUp() throws Exception {
        evaluator = OutputEvaluatorFactory.buildEvaluator("java");
    }

    @After
    public void tearDown() throws Exception {
        evaluator = null;
    }

    @Test
    public void testEvaluate() throws Exception {
        String contents = TestUtils.getFileContents("java-output.txt");
        float score = evaluator.evaluate(contents);

        Assert.assertEquals(score, 20f, 0.1);
    }
}