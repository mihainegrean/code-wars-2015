package com.hp.tools.judge.evaluators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Octavian
 * @since 23.03.2015
 */
public class RegexOutputEvaluator implements OutputEvaluator {
    private Pattern pattern;

    public RegexOutputEvaluator(String regex) {
        pattern = Pattern.compile(regex, Pattern.DOTALL);
    }

    @Override
    public float evaluate(String output) {
        Matcher matcher = pattern.matcher(output);
        if (matcher.find()) {
            float total = Float.parseFloat(matcher.group(1));
            float failures = Float.parseFloat(matcher.group(2));
            return (total - failures) * 100 / total;
        }
        return 0f;
    }
}
