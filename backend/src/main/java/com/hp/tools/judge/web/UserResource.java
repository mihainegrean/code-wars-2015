package com.hp.tools.judge.web;

import com.hp.tools.judge.entities.Solution;
import com.hp.tools.judge.entities.User;
import com.hp.tools.judge.exceptions.OnlineJudgeException;
import com.hp.tools.judge.services.JudgeService;
import com.hp.tools.judge.utils.Constants;
import com.wordnik.swagger.annotations.*;

import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;

/**
 * @author Octavian
 * @since 01.02.2015
 */
@Singleton
@Path("/users")
@Api(value = "Users", position = 0)
public class UserResource {
    private JudgeService service = Constants.JUDGE_SERVICE;

    @Context
    private UriInfo uriInfo;

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Add a new user")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "This user is already registered")
    })
    public Response postUser(
            @ApiParam(value = "The new user details", required = true)
            User user) {

        try {
            service.saveUser(user);
        } catch (OnlineJudgeException e) {
            return Response.status(400).entity(e.getMessage()).build();
        }

        URI uri = uriInfo.getAbsolutePathBuilder().path(Integer.toString(user.getId())).build();
        return Response.created(uri).entity(user).build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Get user by email", response = User.class)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "There is no user with the given email")
    })
    public Response getUserByEmail(
            @ApiParam(value = "The user email", required = true)
            @QueryParam("email")
            String email) {

        User user = service.findUserByEmail(email);
        if (user == null) {
            return Response.status(404).build();
        }
        return Response.ok(user).build();
    }

    @POST
    @Path("/{userId}/problems/{problemId}/language/{lang}")
    @Consumes({MediaType.TEXT_PLAIN})
    @ApiOperation(value = "Send the solution for evaluation")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "The user ID or the problem ID are invalid")
    })
    public Response postSolution(
            @ApiParam(value = "The user ID", required = true)
            @PathParam("userId")
            int userId,
            @ApiParam(value = "The problem ID", required = true)
            @PathParam("problemId")
            int problemId,
            @ApiParam(value = "The language used for the solution", required = true)
            @PathParam("lang")
            String lang,
            @ApiParam(value = "The solution code", required = true)
            String code) {

        Solution solution = new Solution();
        solution.setUserId(userId);
        solution.setProblemId(problemId);
        solution.setLanguage(lang);
        solution.setCode(code);


        try {
            service.saveSolution(solution);
        } catch (OnlineJudgeException e) {
            return Response.status(400).entity(e.getMessage()).build();
        }

        URI uri = uriInfo.getBaseUriBuilder().path("solutions").path(Integer.toString(solution.getId())).build();

        try {
            service.scoreSolution(solution.getId(), Constants.AGENT_SERVICE);
        } catch (OnlineJudgeException ignore) {
        }
        return Response.created(uri).entity(solution).build();
    }
}
