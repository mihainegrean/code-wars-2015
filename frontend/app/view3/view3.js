'use strict';

angular.module('myApp.view3', ['ngRoute', 'ngCookies'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view3', {
    templateUrl: 'view3/view3.html',
    controller: 'problemController'
  });
}])

.controller('problemController', ['$scope', '$location', '$http', '$cookieStore', 'problemsService', 'backendUrlService', function($scope, $location, $http, $cookieStore, problemsService, backendUrlService) {
    
    $scope.backend = backendUrlService.getUrl();
    $scope.problemIndex = problemsService.getProblemIndex();
    var problems = problemsService.getProblems();
    $scope.problem = problems[$scope.problemIndex];
    $scope.codeLanguage = "python";
    $scope.user = $cookieStore.get('name');
    
    $scope.logout = function(view) {
        $location.path(view);
    }
    
    $scope.changeView = function(view) {
        var lang = "";
        if($scope.codeLanguage == "text/x-java") {
            lang = "java";
        } else {
            lang = "python";
        }
        var url = backendUrlService.getUsers() + "/" + $cookieStore.get('id') + "/problems/" + ($scope.problemIndex + 5) + "/language/" + lang;
        var req = {
             method: 'POST',
             url: url,
             headers: {
               'Content-Type': 'text/plain'
             },
             data: $scope.content,
        }
        $http(req).success(function(data) {
             console.log(data);
        });
        $location.path(view);
    }
    
    $scope.content = $scope.problem.methods[1].signature;
    $scope.$watch(function($scope) { return $scope.codeLanguage }, function() {
        if($scope.codeLanguage == "python"){
            $scope.content = $scope.problem.methods[1].signature;
        } else {
            $scope.content = $scope.problem.methods[0].signature;
        }     
    });
}]);