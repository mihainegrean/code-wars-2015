import json

import web

from main.compilers.compiler_defs import JavaCompiler, PythonCompiler
from main.exception.exception_defs import InvalidRequest, UnsupportedLanguage, CompilationError, UnitTestsError, \
    OutOfMemoryError

HELLO_MSG = "Give me some code to Test"
SUPPORTED_LANGUAGES = {"java": JavaCompiler, "python": PythonCompiler}


class TestCode:
    def GET(self):
        return json.dumps({"Status": 0, "Message": HELLO_MSG})

    def POST(self):
        data = web.data()

        json_data = json.loads(data)
        try:
            source_code = json_data["code"]
            programming_language = json_data["language"]
            unit_tests = json_data["testCode"]
        except:
            raise InvalidRequest("Invalid Compilation Request")
        programming_language = programming_language.lower()
        if programming_language not in SUPPORTED_LANGUAGES.keys():
            raise UnsupportedLanguage("Unsupported language: " + programming_language)
        print("=================================================================================================================")
        print("Received valid compilation request:\n Programming Language: " + programming_language + "\n Source Code: " + source_code)
        compiler = SUPPORTED_LANGUAGES.get(programming_language)(source_code, unit_tests)

        (rc, stdout, stderr) = compiler.compile_code()
        print("Compilation Report: %s" % ((rc, stdout, stderr),))
        if rc != 0:
            raise CompilationError("Compile Error: " + stderr)

        (rc, stdout, stderr) = compiler.run_test()
        print("Test Report: %s" % ((rc, stdout, stderr),))
        print("=================================================================================================================")
        if rc != 0:
            if ("java.lang.OutOfMemoryError" in stdout) or ("MemoryError" in stdout):
                raise OutOfMemoryError("Out Of Memory Error: " + stdout)
            else:
                raise UnitTestsError("Unit Testing Error: " + stdout)

        web.header('Content-Type', 'application/json')
        return json.dumps({"returnCode": 0, "stdout": stdout, "stderr": stderr})
